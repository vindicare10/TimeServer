package unit;

import client.ClientTCP;
import client.ClientUDP;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import server.ServerTCP;
import server.ServerUDP;

/**
 * Created by jordan on 30/06/17.
 */
public class TimeoutTest {

    final static Logger logger = Logger.getLogger(TimeoutTest.class);

    @Test
    public void timeoutTestUDP() {

        ServerUDP s = new ServerUDP(9999);
        s.start();

        ClientUDP c = new ClientUDP(9999, "localhost");
        Assert.assertNotEquals(c.requestUDP("date"), "no response from server");

        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("timeoutTestUDP interrupted before completing test:", e);
        }

        Assert.assertEquals(c.requestUDP("date"), "no response from server");
    }

    @Test
    public void timeoutTestTCP() {
        ServerTCP s = new ServerTCP(9999);
        s.start();

        ClientTCP c = new ClientTCP(9999, "localhost");

        String response = c.requestDateTCP();
        Assert.assertNotNull(response);

        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("timeoutTestUDP interrupted before completing test:", e);
        }

        response = c.requestDateTCP();
        Assert.assertEquals(response,"no response from server");
    }
}
