package unit;

import datetime.InternalTime;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Created by jordan on 29/06/17.
 */
public class DateTimeTest {

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern ("yyyy-MM-dd");
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern ("yyyy-MM-dd'T'HH:mm:ss, z");
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern ("HH:mm:ss, z");



    @Test
    public void dateTest() {
        InternalTime it = new InternalTime();

        try {
            String s = it.getCurrentDate();
            dateFormatter.parse(s);
        } catch (DateTimeParseException e) {
            Assert.fail("Parse exception (unexpected format) while testing InternalTime method: getCurrentDate()");
        }
    }

    @Test
    public void timeWithZoneTest() {
        InternalTime it = new InternalTime();

        try {
            String s = it.getCurrentTimeWithZone();
            timeFormatter.parse(s);
        } catch (DateTimeParseException e) {
            Assert.fail("Parse exception (unexpected format) while testing InternalTime method: getCurrentTimeWithZone()");
        }
    }

    @Test
    public void dateTimeWithZoneTest() {
        InternalTime it = new InternalTime();

        try {
            String s = it.getCurrentDateTimeWithZone();
            dateTimeFormatter.parse(s);
        } catch (DateTimeParseException e) {
            Assert.fail("Parse exception (unexpected format) while testing InternalTime method: getCurrentDateTimeWithZone()");
        }
    }
}
