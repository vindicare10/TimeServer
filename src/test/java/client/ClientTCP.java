package client;

import java.io.*;
import java.net.Socket;

/**
 * Created by jordan on 30/06/17.
 */
public class ClientTCP {

    private int serverPort;
    private String serverIP;

    public ClientTCP(int port, String ip) {
        this.serverIP = ip;
        this.serverPort = port;
    }

    public String requestDateTCP() {

        String response = "no response from server";
        try {
            Socket clientSocket = new Socket(this.serverIP, this.serverPort);
            clientSocket.setSoTimeout(5000);

            BufferedWriter writeToServer = new BufferedWriter( new OutputStreamWriter( clientSocket.getOutputStream() ) );
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


            writeToServer.write( "date" );
            writeToServer.newLine();
            writeToServer.flush();


            response = inFromServer.readLine();

            writeToServer.close();
            inFromServer.close();

            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }
}
