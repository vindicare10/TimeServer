package client;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.*;

/**
 * Created by jordan on 30/06/17.
 */
public class ClientUDP {

    final static Logger logger = Logger.getLogger(ClientUDP.class);

    private int serverPort;
    private String serverIP;

    public ClientUDP(int port, String ip) {
        this.serverIP = ip;
        this.serverPort = port;
    }

    public String requestUDP(String request) {

        String responseString = "no response from server";

        try {
            DatagramSocket clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(5000);
            InetAddress address = InetAddress.getByName(serverIP);

            byte[] sendData = new byte[1024];
            byte[] receiveData = new byte[1024];

            sendData = request.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, serverPort);
            clientSocket.send(sendPacket);

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            responseString = new String(receivePacket.getData());

            clientSocket.close();
        } catch (SocketTimeoutException e) {
            logger.warn("UDP client timed out after 5 seconds of inactivity:", e);
        } catch(SocketException e) {
            // TODO : catch exception
            e.printStackTrace();
        } catch (UnknownHostException e) {
            // TODO : catch exception
            e.printStackTrace();
        } catch (IOException e) {
            // TODO : catch exception
            e.printStackTrace();
        }

        return responseString;
    }




}
