package datetime;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by jordan on 29/06/17.
 */
public class InternalTime {

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern ("yyyy-MM-dd");
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern ("yyyy-MM-dd'T'HH:mm:ss, z");
    private DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern ("HH:mm:ss, z");

    private ZoneId zid;

    public InternalTime() {
        this.zid = ZoneId.of( "America/Montreal");
    }

    public InternalTime(ZoneId zid) {
        this.zid = zid;
    }

    public String getCurrentDate() {
        ZonedDateTime zdt = ZonedDateTime.now(this.zid);
        return dateFormatter.format(zdt);
    }

    public String getCurrentTimeWithZone() {
        ZonedDateTime zdt = ZonedDateTime.now(this.zid);
        return timeFormatter.format(zdt);
    }

    public String getCurrentDateTimeWithZone() {
        ZonedDateTime zdt = ZonedDateTime.now(this.zid);
        return dateTimeFormatter.format(zdt);
    }
}
