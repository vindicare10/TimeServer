package server;

/**
 * Created by jordan on 29/06/17.
 */
public class TimeServer extends Thread {

    private int port;

    public TimeServer(int port) {
        this.port = port;
    }

    @Override
    public void run() {
        ServerUDP sUDP = new ServerUDP(this.port);
        sUDP.start();

        ServerTCP sTCP = new ServerTCP(this.port);
        sTCP.start();
    }
}
