package server;

import datetime.InternalTime;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;

/**
 * Created by jordan on 30/06/17.
 */
public class ServerUDP extends Thread {
    final static Logger logger = Logger.getLogger(ServerUDP.class);

    private static byte[] DATE_REQUEST = "date".getBytes();
    private static byte[] TIME_REQUEST = "time".getBytes();
    private static byte[] DATETIME_REQUEST = "datetime".getBytes();

    private InternalTime it;

    private int port;

    public ServerUDP(int port) {
        this.port = port;
        it = new InternalTime();
    }

    @Override
    public void run() {
        try(DatagramSocket ds = new DatagramSocket(port)) {
            ds.setSoTimeout(5000);

            byte[] receivedData = new byte[DATETIME_REQUEST.length];
            byte[] sendData;

            while(true) {
                DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);
                ds.receive(receivedPacket);

                if(receivedData[0]==100 && receivedData[1]==97 && receivedData[2]==116 && receivedData[3]==101
                   && receivedData[4]==116 && receivedData[5]==105 && receivedData[6]==109 && receivedData[7]==101) {
                    sendData = it.getCurrentDateTimeWithZone().getBytes();
                } else if(receivedData[0]==100 && receivedData[1]==97 && receivedData[2]==116 && receivedData[3]==101) {
                    sendData = it.getCurrentDate().getBytes();
                } else if(receivedData[0]==116 && receivedData[1]==105 && receivedData[2]==109 && receivedData[3]==101) {
                    sendData = it.getCurrentTimeWithZone().getBytes();
                } else {
                    continue;
                }

                InetAddress a = receivedPacket.getAddress();
                int p = receivedPacket.getPort();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, a, p);
                ds.send(sendPacket);
            }
        } catch (SocketTimeoutException e) {
            logger.warn("UDP datagram socket timed out after 5 seconds of inactivity:", e);
        } catch (IOException e) {
            logger.error("IOException caught while handling UDP packet:", e);
        }
    }
}
