package server;

import datetime.InternalTime;
import org.apache.log4j.Logger;

import javax.swing.text.html.Option;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by jordan on 29/06/17.
 */
public class ConnectionHandlerTCP implements Runnable {

    final static Logger logger = Logger.getLogger(ConnectionHandlerTCP.class);

    private Socket clientSocket;
    private InternalTime it;

    public ConnectionHandlerTCP(Socket clientSocket) {
        this.clientSocket = clientSocket;
        this.it = new InternalTime();
    }

    public void run() {

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            DataOutputStream out = new DataOutputStream(clientSocket.getOutputStream());

            String request;
            while((request = in.readLine()) != null) {
                String s = "";

                if (request.equals("date")) {
                    s = it.getCurrentDate() + '\n';
                } else if(request.equals("time")) {
                    s = it.getCurrentTimeWithZone() + '\n';
                } else if(request.equals("datetime")) {
                    s = it.getCurrentDateTimeWithZone() + '\n';
                } else {
                    System.out.println(request);
                }

                if(s.length()>0) {
                    out.writeBytes(s);
                }
            }

            in.close();
            out.flush();
            out.close();

            clientSocket.close();
        } catch (IOException e) {
            logger.error("IOException caught while handling TCP connection:", e);
        }
    }
}
