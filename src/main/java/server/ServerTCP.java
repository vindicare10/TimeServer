package server;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by jordan on 30/06/17.
 */
public class ServerTCP extends Thread {

    final static Logger logger = Logger.getLogger(ServerTCP.class);

    private BlockingQueue<Socket> tcpConnections;

    private Thread dispatcher;

    private int port;

    public ServerTCP(int port) {
        this.port = port;
        this.tcpConnections = new LinkedBlockingQueue<Socket>();

        dispatcher = new Thread(new ConnectionDispatcherTCP(this.tcpConnections));
        dispatcher.start();
    }

    @Override
    public void run() {
        try {
            ServerSocket ss = new ServerSocket(port);
            ss.setSoTimeout(5000);

            while (true) {
                Socket connection = ss.accept();
                tcpConnections.put(connection);
            }
        } catch (SocketTimeoutException e) {
            logger.warn("TCP server socket timed out after 5 seconds of inactivity:", e);
            dispatcher.interrupt();
        } catch (IOException e) {
            logger.error("IOException caught while serving tcp socket connections:", e);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            dispatcher.interrupt();
            logger.warn("TCP server interrupted:", e);
        }
    }
}
