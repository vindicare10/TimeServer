package server;

import org.apache.log4j.Logger;

import java.net.Socket;
import java.util.concurrent.BlockingQueue;

/**
 * Created by jordan on 29/06/17.
 */
public class ConnectionDispatcherTCP implements Runnable {

    final static Logger logger = Logger.getLogger(ConnectionDispatcherTCP.class);

    private BlockingQueue<Socket> tcpConnections;

    public ConnectionDispatcherTCP(BlockingQueue<Socket> tcpConnections) {
        this.tcpConnections = tcpConnections;
    }

    public void run() {
        try {
            while(true) {
                Socket s = tcpConnections.take();
                new Thread(new ConnectionHandlerTCP(s)).start();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.warn("TCP server interrupted:", e);
        }
    }
}
