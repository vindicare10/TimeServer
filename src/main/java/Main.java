import org.apache.log4j.Logger;
import server.TimeServer;

/**
 * Created by jordan on 30/06/17.
 */
public class Main {
    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        if(args.length == 1) {
            try {
                int port = Integer.parseInt(args[0]);
                TimeServer ts = new TimeServer(port);
                ts.start();
            } catch (NumberFormatException e) {
                logger.error("Non integer argument passed as port:", e);
                System.out.println("Usage: java TimeServer [port number]");
                System.out.println("replace [port number] with integer greater than 0");
            }
        } else {
            System.out.println("Usage: java TimeServer [port number]");
        }
    }
}
